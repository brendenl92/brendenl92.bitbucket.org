// ICT 4510
// Kristy McNulty
// 06/1/17
// This script uses Flickr API and AJAX GET request to grab pictures from JSON file and display them on the page

var apiurl = "https://api.flickr.com/services/feeds/photos_public.gne?tags=restaurants&format=json&jsoncallback=?";
$.getJSON(apiurl,function(json)
{
  $.each(json.items ,function(i,myresult)
  {
     $("#results").append('<li class="gallery-items"><a href="'+myresult.link+'" target="_blank"><img src="'+myresult.media.m+'"/></a></li>');
  });
});
