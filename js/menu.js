(function ()
{
	if(localStorage.getItem("menu") === null)
	{
		console.log("Getting JSON");
		$.getJSON("json/menu.json", function(result)
		{
			console.log("Json retrieved.")
        	//store JSON in localStorage
       		localStorage.setItem('menu', JSON.stringify(result));
       		showMenu(result);
		});
	}
	else
	{
		console.log("json is cached");
		showMenu(JSON.parse(localStorage.getItem('menu')));
	}
})();

function showMenu(menu)
{
	var list = "";

	$.each(menu.course, function(i, thisCourse)
	{
		list += "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
		list += "<div class='course'>";
		list += "<div class='courseheader'><u>";
		list += thisCourse.name;
		list += "</u></div>";
		list += "<div class='row'>";
		$.each(thisCourse.dishes, function(j, thisDish)
		{
			if(j % 2 == 0 && j != 0 && j != thisCourse.dishes.length - 1 )
			{
				list += "</div>";
				if(j != thisCourse.dishes.length - 1)
					list += "<div class='row'>";
			}
			list += "<div class='col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12'>";
			list += "<div class='dish'>";
			list += "<div class='dishname'>";
			list += thisDish.name;
			list += "</div> <div class='dishdesc'>";
			list += thisDish.desc;
			list += "</div> <div class='dishprice'>";
			list += thisDish.price;
			list += "</div></div></div>";
		});
		list += "</div></div></div>";
	});
	$("#menu").append(list);
};
