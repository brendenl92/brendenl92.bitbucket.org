// ICT 4510
// Kristy McNulty
// 05/30/2017
// This script uses jQuery library to validate input by the user, and then stores the form values entered by the user
// and displays a confirmation message to the user to ensure form has been submitted 

(function ()
{
  $("#form").validate(
  {
    submitHandler: function(form)
    {
      var values = getFormValues();
      var url ="process.php";
      $.post(url, values, function(json)
      {
        displayMessage(json);
      });
    }
  });
})();

function getFormValues()
{
  var formValues = {};

  formValues.firstName = $('#firstName').val();
  formValues.lastName = $('#lastName').val();
  formValues.email = $('#email').val();
  formValues.phone = $('#phone').val();

  return formValues;
}

function displayMessage(json)
{
  var display = $('#contact-form');
  var data = '<p>' + json.message + '</p>';

  display.empty().append(data);
  $('form').fadeOut('slow');
}
