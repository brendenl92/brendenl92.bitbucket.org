// Kristy McNulty
// ICT 4510
// May 25, 2017
// Uses slick library to create a jQuery carousel that cotninually flashes content in div class 'center'

  $(document).ready(function(){
  $('.center').slick({
    arrows: false,
    dots: true,
    autoplay: true
  });
});
