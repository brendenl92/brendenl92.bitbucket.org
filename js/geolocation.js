// ICT 4510
// Kristy McNulty
// 05/25/2017
// Displays restaurant's location, coordinates, and red marker on a responsive map using Google APIs

var restaurantCoords = {lat: 30.702405, lng: -96.323354};

function showMap ()
{
  console.log("We're in showmap, coords are " + restaurantCoords.lat +", " + restaurantCoords.lng);
  var map = new google.maps.Map(document.getElementById('map'),
    {
      zoom: 10,
      center: restaurantCoords
    });

  var marker = new google.maps.Marker(
    {
      position: restaurantCoords,
      map: map
    });

  var markerContent = "Giovanni's is located here: " + restaurantCoords.lat +", " + restaurantCoords.lng;

  var infoWindowOptions =
  {
    content: markerContent,
    position: map.getCenter()
  };

  var infoWindow = new google.maps.InfoWindow(infoWindowOptions);

  google.maps.event.addListener(marker, 'click',
  function()
  {
    infoWindow.open(map);
  });

  google.maps.event.addDomListener(window, "resize", function()
  {
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
  });
}
