<?php include("header.php"); ?>

<body>
<?php $thisPage = "menu"; ?>
<?php include("navigation.php"); ?>

  <h1>Menu</h1>
  <p class="h1-subtext">Take a peek at our available menu items below!</p>
  <div id="menu" class="container"></div>

<?php include("footer.php"); ?>

 <!-- javascript calls -->

<script src="js/menu.js"></script>
</body>
</html>
