<div class="container-fluid">
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Giovanni's Italian Ristorante</a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a <?php if($thisPage == "home") echo " class='btn btn-default btn-outline btn-circle collapsed' data-toggle='collapse' href='#' aria-expanded='false' aria-controls=' nav-collapse1'"; else echo "href='index.php'";?>>Home</a></li>
            <li><a <?php if($thisPage == "about") echo " class='btn btn-default btn-outline btn-circle collapsed' data-toggle='collapse' href='#' aria-expanded='false' aria-controls='nav-collapse1' "; else echo "href='about.php'";?>>About</a></li>
            <li><a <?php if($thisPage == "menu") echo " class='btn btn-default btn-outline btn-circle collapsed' data-toggle='collapse' href='#' aria-expanded='false' aria-controls='nav-collapse1' "; else echo "href='menu.php'";?>>Menu</a></li>
            <li><a <?php if($thisPage == "gallery") echo " class='btn btn-default btn-outline btn-circle collapsed' data-toggle='collapse' href='#' aria-expanded='false' aria-controls='nav-collapse1' "; else echo "href='gallery.php'";?>>Gallery</a></li>
            <li><a <?php if($thisPage == "contact") echo " class='btn btn-default btn-outline btn-circle collapsed' data-toggle='collapse' href='#' aria-expanded='false' aria-controls='nav-collapse1' "; else echo "href='contact.php'";?>>Contact</a></li>
          </ul>
        </div>
      </div>
    </nav>
</div>
