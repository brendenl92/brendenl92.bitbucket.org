<?php include("header.php"); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkn9yfrHAyRuZWupaYgwwkGpwQeE00Rq4&callback=showMap" async defer></script>
<script src="js/geolocation.js"></script>
<body>
<?php $thisPage = "about"; ?>
<?php include("navigation.php"); ?>

<h1>About Us</h1>
  <div class="container">
    <div class="row-fluid">
      <div class="col-md-6">
        <div class="about">
          <p>Contact us at:
          <br>(555)555-5555
          <br>contact@giovannis.com</p>
        </div>
        <div class="about">
          <p>Hours of Operation:
            <br> Monday - Thursday: 11 AM to 10 PM
            <br> Friday - Saturday: 10 AM to 11 PM
            <br> Sunday: 10 AM to 9 PM</p>
        </div>
        <div class="about">
          <p>Location:
            <br>4545 Old Reliance Rd
            <br>Bryan, TX 77802</p>
            <p>Check out the map to find us!</p>
        </div>
      </div>
      <div class="col-md-6">
        <div id="map" class="map">
          Google Map
        </div>
      </div>
    </div>
  </div>

<?php include("footer.php"); ?>

</body>
</html>
