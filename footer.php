<footer>
   <div class="footer" id="footer">
       <div class="container">
           <div class="row">
               <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                   <h3>Find</h3>
                   <ul>
                       <li> <a href="index.html">Home Page</a> </li>
                       <li> <a href="menu.html">List of Items On Our Menu</a> </li>
                       <li> <a href="gallery.html">Image Gallery</a> </li>
                       <li> <a href="about.html">About Giovanni's</a> </li>
                       <li> <a href="contact.html">Contact The Restaurant</a> </li>
                   </ul>
               </div>

               <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                   <h3>Visit Us</h3>
                   <ul>
                       <li>1000 Bistro Square Pkwy
                           <br>College Station, TX 77840</a> </li>
                           <br>
                       <li>Hours of Operation:
                           <br> Mon - Thurs: 11 AM to 10 PM
                           <br> Fri - Sat: 10 AM to 11 PM
                           <br> Sun: 10 AM to 9 PM</a> </li>
                   </ul>
               </div>

               <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                   <h3>Get In Touch</h3>
                   <ul>
                       <li>
                           <div class="input-append newsletter-box text-center">
                               <input type="text" class="full text-center" placeholder="Email ">
                               <button class="btn  bg-gray" type="button"> contact@giovannis.com <i class="fa fa-long-arrow-right"> </i> </button>
                           </div>
                       </li>
                   </ul>
                   <ul class="social">
                       <li> <a href="#"> <i class=" fa fa-facebook">   </i> </a> </li>
                       <li> <a href="#"> <i class="fa fa-twitter">   </i> </a> </li>
                       <li> <a href="#"> <i class="fa fa-pinterest">   </i> </a> </li>
                   </ul>
               </div>
           </div>
       </div>
   </div>

   <div class="footer-bottom">
       <div class="container">
           <p class="pull-left">University of Denver ICT4510 Final Project || Copyright 2017© Giovanni's Italian Ristorante. All rights reserved.</p>
       </div>
   </div>
</footer>
