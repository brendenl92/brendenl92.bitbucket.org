
<?php include("header.php"); ?>

<body>
<?php $thisPage = "contact"; include("navigation.php"); ?>

  <h1>Contact Us</h1>
  <p class="h1-subtext">If you have any questions or concerns, please fill out the form below. We'd love to hear from you!</p>

  <!-- contact form  -->
    <div class="container" id="contact-container">
        <div class="row">
              <div class="col-md-12" id="contact-form">
                    <form class="form-horizontal" id="form" method="post">
                        <fieldset>

                            <div class="form-group">
                                <span class="col-md-1 text-center"><i class="fa fa-user bigicon"></i></span>
                                <div class="col-xs-12">
                                    <input id="firstName" name="lname" type="text" placeholder="First Name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 text-center"><i class="fa fa-user bigicon"></i></span>
                                <div class="col-xs-12">
                                    <input id="lastName" name="fname" type="text" placeholder="Last Name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                                <div class="col-xs-12">
                                    <input id="email" name="email" type="email" placeholder="Email Address" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                <div class="col-xs-12">
                                    <input id="phone" name="phone" type="tel" placeholder="Phone" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-1 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                                <div class="col-xs-12">
                                    <textarea class="form-control" id="message" name="message" placeholder="Enter your message for us here." rows="7"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-xs-12 text-center">
                                    <button id "submitButton" type="submit" class="btn btn-primary btn-lg">Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
              </div>
            </div>
            <p id="display"></p>
    </div>

<?php include("footer.php"); ?>

 <script src="js/main.js"></script>
</body>
</html>
