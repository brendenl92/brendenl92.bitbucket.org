<?php include("header.php"); ?>

<body>
<?php $thisPage = "home"; ?>
<?php include("navigation.php"); ?>

      <!-- image container -->
      <div class="container-fluid">
        <div class="col-md-12 nopadding">
          <img src="images/restaurant-banner.jpeg" style="width: 100%;" alt="Giovanni's" class="img-responsive" />
          <!-- Image credit to Pantaloni Torino  via http://www.pt-pantalonitorino.it/il-vino-entra-nella-spa-di-lusso-2/ -->
        </div>
      </div>
    <!-- jumbotron welcome -->
    <div class="jumbotron">
      <h1>Welcome to Giovanni's</h1>
        <p>Home of the finest Italian fare in Aggieland!</p>
    </div>
    <!-- jQuery carousel -->
  <div class="width-fix">
    <div class="center">
      <div class="card">
      <h4 class="card-title">Only The Freshest Ingredients</h4>
      <img class="img-responsive center-block" src="images/pasta-grid.jpg" alt="Card image cap"/>
      <!-- Image credit to David Lebovitz via http://www.davidlebovitz.com/how-to-make-fresh-pasta-homemade-recipe/ -->
          <div class="card-block">
            <p class="card-text">Always cooked in-house, handmade by our award-winning chefs.</p>
        </div>
      </div>
      <div class="card">
      <h4 class="card-title">Choose From Over 50 Fine Wines</h4>
      <img class="img-responsive center-block" src="images/wine-grid.jpg" alt="Card image cap"/>
      <!-- Image credit to Divino Bistro via https://divinobistro.com/about/ -->
          <div class="card-block">
            <p class="card-text">From modest moscatos to intricate red blends, and everything in between.</p>
        </div>
      </div>
      <div class="card">
      <h4 class="card-title">World Class Customer Service</h4>
      <img class="img-responsive center-block" src="images/service-grid.jpg" alt="Card image cap"/>
      <!-- Image credit to SheSpeaks via http://www.shespeaks.com/Are-Full-Service-Restaurants-Just-As-Bad-For-Your-Health-As-Fast-Food-Chains -->
          <div class="card-block">
            <p class="card-text">Waitstaff that serves above and beyond, tailored to your needs.</p>
        </div>
      </div>
    </div>
  </div>
    <!-- spacer container -->
    <div class="help-block"></div>

  <?php include("footer.php"); ?>

  <!--javascript calls -->
  <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
  <script type="text/javascript" src="slick/slick.js"></script>
  <script type="text/javascript" src="js/carousel.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>
