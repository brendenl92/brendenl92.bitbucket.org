<!DOCTYPE html>
<html lang="en-us">
<head>
  <title>Final Project</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/footer.css">
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
  <script src="https://use.fontawesome.com/51f414be20.js"></script>
  <script src="js/jquery-3.2.1.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/jquery.validate.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
