<?php include("header.php"); ?>

<body>
<?php $thisPage = "gallery"; ?>
<?php include("navigation.php"); ?>

  <h1>Gallery</h1>
  <ul id ="results"></ul>

<?php include("footer.php"); ?>

 <!-- javascript calls -->
 <script src="js/gallery.js"></script>
</body>
</html>
